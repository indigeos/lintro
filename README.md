# Lintro
Lintro is a Linux distribution used for study of Linux. It contains introduced defects and exercises for an introduction to Linux perspective.

Lintro is based off of Debian Linux and contains some packages for simple graphical use. Its primary function is to be a Linux version that is accessible to students. Its secondary focus is to be small so that it is downloadable as a VM. Its tertiary target is to be useable on multiple target machines usable by upper division high school or freshman/sophmore college student.

## Getting started
Debian: Start with the debian net-install version. Use a minimal install method.

Post install, install the following packages using apt:

```
```

Run the script to setup users. 

Copy over the script over from a different Lintro /root directory. XFDesktop. 

Install a small browser (firefox is ok, but kinda big)

Copy the looper source. Build it and test it. Remove the source and build tools.
FUTURE: (build this in distro release and package)

Use bleachbit to remove extra data (prep for release)
```bleachbit -l | grep -E "[a-z0-9_\-]+\.[a-z0-9_\-]+" | xargs bleachbit -c```

As root, defrag the disk.
```e4defrag /```

Write out zeros to the rest of the disk. This makes it compressable under VM.
```dd if=/dev/zero of=wipefile bs=1M; sync; rm wipefile```

[Shrink the VM](https://kb.vmware.com/s/article/1001934)
```vmware-toolbox-cmd disk shrink /```
